import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class JuiceTest {

        @Test
        void personTest() {

            Juice juice = new Juice("orange", 87, 4);

            assertEquals("orange", juice.getTaste());
            assertEquals(87, juice.getPrice());

            juice.decrementShelfLife();
            juice.decrementShelfLife();

            assertEquals(67, juice.getPrice());
        }

}
