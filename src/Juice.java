public class Juice {

        private String taste;
        private int price;
        private int shelfLife;

        public Juice (String taste, int price, int shelfLife) {

                if (taste == null || taste.isBlank()){
                        throw new IllegalArgumentException("Taste is null or empty");
                }

                if (price < 0){
                        throw new IllegalArgumentException("Price is negative");
                } else if (price == 0) {
                        throw new IllegalArgumentException("Price is zero");
                }

                if (shelfLife < 0){
                        throw new IllegalArgumentException("ShelfLife is negative");
                } else if (shelfLife == 0){
                        throw new IllegalArgumentException("ShelfLife is zero");
                }

                this.taste = taste;
                this.price = price;
                this.shelfLife = shelfLife;
        }

        public String getTaste() {
                return taste;
        }

        public int getPrice() {
                return price;
        }

        public void decrementShelfLife() {
                this.shelfLife--;
        }

        public void priceReduction() {
                if (this.price > 20 && this.shelfLife == 2) {
                        this.price -= 20;
                }
        }

}